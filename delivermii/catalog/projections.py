from catalog.store import Store
from cqrs.projections import Projection, ProjectionBase
from sqlalchemy import Column, String


class StoreProjection(ProjectionBase, Projection):
    __tablename__ = 'stores'

    id = Column(String(250), primary_key=True)
    name = Column(String(250), nullable=False, unique=True)

    @staticmethod
    def project(obj: Store):
        return StoreProjection(
            id=str(obj.id),
            name=obj.name
        )
