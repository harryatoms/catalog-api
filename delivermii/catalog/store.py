from cqrs.aggregates import Aggregate
from cqrs.commands import Command
from overloading import *


class RegisterStore(Command):
    """
        Create Store Command
    """
    @property
    def aggregate_type(self):
        return Store


class Store(Aggregate):
    """
        Store Aggregate
    """
    def __init__(self, aggregate_id):
        super().__init__(aggregate_id)
        self.name = None
        self.created_at = None

    @overloads(Aggregate.handle_cmd)
    def handle_cmd(self, cmd: RegisterStore):
        """
            Register new Store
        :param cmd: RegisterStore
        """
        self.id = cmd.aggregate_id
        self.name = cmd.payload.get('name')
