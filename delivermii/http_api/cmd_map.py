from catalog.store import RegisterStore, Store
from catalog.projections import StoreProjection


PROJECTIONS = {
    Store.__name__: [StoreProjection]
}
