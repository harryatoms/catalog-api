from bottle import route, request


@route('/commands/<cmd_name>', method='POST')
def process_command(cmd_name, command_factory, command_processor, req=request):
    cmd = command_factory.make_cmd(cmd_name, data=req.json)
    command_processor.process(cmd)
    return "ID: {aid}".format(aid=cmd.aggregate_id)
