from cqrs.cpu import CommandProcessor
from cqrs.commands import CommandFactory
from cqrs.repository import AggregateRepository
from cqrs.projections import ProjectionBase, Projector
from http_api.cmd_map import PROJECTIONS
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import bottle_inject
import bottle

# cfg
port = 8000

# db
engine = create_engine('postgres://postgres:postgres@127.0.0.1/delivermii')
Session = sessionmaker(bind=engine)


# manage projections
from http_api import projections
ProjectionBase.metadata.create_all(engine)

# create bottle application
app = bottle.default_app()
di = app.install(bottle_inject.Plugin())


# dependency injection
@di.provider('command_processor')
def provide_cpu():
    return CommandProcessor(AggregateRepository(), Projector(Session(), PROJECTIONS))


@di.provider('command_factory')
def provide_command_factory():
    from catalog import store
    return CommandFactory()


# routes
from http_api import controllers


if __name__ == '__main__':
    # run bottle application
    bottle.run(host='0.0.0.0', port=port, server='gunicorn', workers=1)
    print("Listening on  {p}".format(p=port))
