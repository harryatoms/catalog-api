#
# Setup NGINX
#
# install nginx package
package "nginx-extras"

# remove default site
link "/etc/nginx/sites-enabled/default" do
  action :delete
end

# restart service
service "nginx" do
    action [ :restart ]
end
