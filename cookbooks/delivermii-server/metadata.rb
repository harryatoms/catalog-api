name "delivermii-server"
depends "git"
depends "sudo"
depends "python"
depends "supervisor"
depends "database"
depends "delivermii-postgres"
depends "delivermii-nginx"