#
# Base System
#

# install git for pulling requirements
include_recipe "git::default"

# update apt repos; set non-interactive env var
execute "apt-get update"
execute "apt-get -yq upgrade" do
    environment 'DEBIAN_FRONTEND' => 'noninteractive'
end