#
# Install Python Dependencies
#

# install python
package 'libpq-dev'
package 'python3'
package 'python3-pip'

execute 'python_dependencies' do
    command 'pip3 install -r requirements.txt'
    cwd '/usr/src/app'
end