#
# Delivermii App Server
#
include_recipe 'delivermii-server::_base'
include_recipe 'delivermii-server::_python'
include_recipe 'delivermii-nginx::default'
include_recipe "supervisor::default"


#
# Python Application
#
# api system service
supervisor_service "delivermii_server" do
    action :enable
    autostart true
    autorestart true
    user 'root'
    directory '/usr/src/app/delivermii'
    command '/usr/local/bin/gunicorn -w 1 -b :8000 --reload app:app'
end

#
# NGINX reverse proxy
#
# site configuration
template "delivermii-server.nginx.conf" do
    path "/etc/nginx/sites-available/delivermii-server"
    source "delivermii-server.nginx.conf.erb"
    owner "root"
    group "root"
    mode 00644
end

# link to available sites
link "/etc/nginx/sites-enabled/delivermii-server" do
    to "/etc/nginx/sites-available/delivermii-server"
end

# restart httpd
service "nginx" do
    action [ :restart ]
end