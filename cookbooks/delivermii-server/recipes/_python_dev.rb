#
# Install Python Dependencies
#
execute 'python_dependencies' do
    command 'pip3 install -r /usr/src/app/requirements-dev.txt'
end
