default['postgresql']['version']  =                     '9.4'

default['postgresql']['dir'] =                          "/etc/postgresql/#{default['postgresql']['version']}/main"
default['postgresql']['client']['packages'] =           ["postgresql-client-#{default['postgresql']['version']}", "libpq-dev"]
default['postgresql']['server']['packages'] =           ["postgresql-#{default['postgresql']['version']}"]
default['postgresql']['contrib']['packages'] =          ["postgresql-contrib-#{default['postgresql']['version']}"]

default['postgresql']['config']['data_directory'] =     "/var/lib/postgresql/#{default['postgresql']['version']}/main"
default['postgresql']['config']['hba_file'] =           "/etc/postgresql/#{default['postgresql']['version']}/main/pg_hba.conf"
default['postgresql']['config']['ident_file'] =         "/etc/postgresql/#{default['postgresql']['version']}/main/pg_ident.conf"
default['postgresql']['config']['external_pid_file'] =  "/var/run/postgresql/#{default['postgresql']['version']}-main.pid"

# postgresql server configuration
default['postgresql']['enable_pgdg_apt'] = true
default['postgresql']['config']['listen_addresses'] = '*'
default['postgresql']['pg_hba'] = [
    {:type => 'host', :db => 'all', :user => 'all', :addr => '0.0.0.0/0', :method => 'md5'}
]

# password defaults to 'postgres' for user 'postgres'
default['postgresql']['password']['postgres'] = "postgres"
