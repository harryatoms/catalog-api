# POSTGRESQL
package 'build-essential'
include_recipe 'postgresql::ruby'
include_recipe 'postgresql::client'
include_recipe 'postgresql::server'
include_recipe 'database::postgresql'

# used for db commands below
postgresql_connection_info = {
  :host     => '127.0.0.1',
  :username => 'postgres',
  :password => 'postgres'
}

# create app db
postgresql_database 'delivermii' do
  connection postgresql_connection_info
  action     :create
end

# give privs to postgres user
postgresql_database_user 'postgres' do
  connection postgresql_connection_info
  database_name 'delivermii'
  privileges [:all]
  action :grant
end